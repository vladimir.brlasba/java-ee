package com.cgi.servlets;

import java.io.IOException;
import java.util.*;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.api.PersonNewUserDto;
import com.cgi.exceptions.ResourceNotFoundException;
import com.cgi.services.PersonService;

/**
 * Servlet implementation class PersonServlet
 */
@WebServlet(name = "PersonServlet", urlPatterns = { "/person/*" })
public class PersonServlet extends HttpServlet {
	
	@EJB
	private PersonService personService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if(pathInfo != null && pathInfo.contains("/detail"))
		{
			String idString = request.getParameter("id");
			Long idLong = Long.parseLong(idString);
			try {
				PersonDetailedViewDto person =personService.findById(idLong);
				request.setAttribute("person", person);
				request.getRequestDispatcher("/WEB-INF/jsp/person/Person.jsp").forward(request, response);
				
			}catch(Exception e)
			{
				throw new ResourceNotFoundException(e.getMessage());
			}
		}
		else if (pathInfo != null && pathInfo.contains("/list"))
		{
			try {
				List<PersonBasicViewDto> people = personService.findAll();
			request.setAttribute("persons", people);
			request.getRequestDispatcher("/WEB-INF/jsp/person/PersonList.jsp").forward(request, response);
			}
			catch(Exception e){
				throw new ResourceNotFoundException(e.getMessage());
			}
			
		}
		else if (pathInfo != null && pathInfo.contains("/remove"))
		{
			String idString = request.getParameter("id");
			Long idLong = Long.parseLong(idString);
			try {
				Long deletedPerson = personService.deletePersonById(idLong);
				
				request.setAttribute("person", deletedPerson);
				request.getRequestDispatcher("/WEB-INF/jsp/person/PersonDelete.jsp").forward(request, response);
				
			}catch(Exception e)
			{
				throw new ResourceNotFoundException(e.getMessage());
			}
		}
		else if (pathInfo != null && pathInfo.contains("/personCreate"))
		{
			request.getRequestDispatcher("/WEB-INF/jsp/person/PersonCreate.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonNewUserDto newUser = new PersonNewUserDto();
		newUser.setName(request.getParameter("nickname"));
		newUser.setFamilyName(request.getParameter("familyName"));
		newUser.setGivenName(request.getParameter("givenName"));
		if(newUser.getName().equals(null)|| newUser.getGivenName().equals(null) || newUser.getFamilyName().equals(null))
		{
			request.getRequestDispatcher("/WEB-INF/jsp/person/PersonCreate.jsp").forward(request, response);
		}
		else
		{
			PersonBasicViewDto newPerson = personService.createUser(newUser);
			System.out.println(newUser);
			request.setAttribute("person", newPerson);
			request.getRequestDispatcher("/WEB-INF/jsp/person/PersonNew.jsp").forward(request, response);
		}
		
	}

}
