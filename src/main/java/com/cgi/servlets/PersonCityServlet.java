package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.api.PersonFromCityDto;
import com.cgi.exceptions.ResourceNotFoundException;
import com.cgi.services.PersonService;

/**
 * Servlet implementation class PersonCityServlet
 */
@WebServlet(name = "PersonCityServlet", urlPatterns = { "/city/*" })
public class PersonCityServlet extends HttpServlet {

	@EJB
	private PersonService personService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonCityServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		System.out.println("Path: " + pathInfo);
		if (pathInfo != null && pathInfo.contains("/brno")) {
			String city = "Brno";
			try {
				List<PersonFromCityDto> people = personService.findByCity(city);
				request.setAttribute("persons", people);
				request.getRequestDispatcher("/WEB-INF/jsp/city/Brno.jsp").forward(request, response);

			} catch (Exception e) {
				throw new ResourceNotFoundException(e.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/praha")) {
			String city = "Praha";
			try {
				List<PersonFromCityDto> people = personService.findByCity(city);
				request.setAttribute("persons", people);
				request.getRequestDispatcher("/WEB-INF/jsp/city/Praha.jsp").forward(request, response);

			} catch (Exception e) {
				throw new ResourceNotFoundException(e.getMessage());
			}
		} else if (pathInfo != null && pathInfo.contains("/plzen")) {
			String city = "Plzen";
			try {
				List<PersonFromCityDto> people = personService.findByCity(city);
				request.setAttribute("persons", people);
				request.getRequestDispatcher("/WEB-INF/jsp/city/Plzen.jsp").forward(request, response);

			} catch (Exception e) {
				throw new ResourceNotFoundException(e.getMessage());
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
