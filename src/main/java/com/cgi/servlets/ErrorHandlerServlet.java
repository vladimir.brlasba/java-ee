package com.cgi.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.ApiError;
import com.cgi.exceptions.ResourceNotFoundException;

/**
 * Servlet implementation class ErrorHandlerServlet
 */
@WebServlet(name = "ErrorHandlerServlet", urlPatterns = { "/error-handler" })
public class ErrorHandlerServlet extends HttpServlet {

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ErrorHandlerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
		
		ApiError apiError = new ApiError();
		if (throwable instanceof ResourceNotFoundException) {
			ResourceNotFoundException ex = (ResourceNotFoundException) throwable;
			apiError.setTimestamp(System.currentTimeMillis());
			apiError.setStatusCode(404);
			apiError.setMessage(ex.getMessage());
			apiError.setPath(request.getRequestURI());
			apiError.setError("...error...");
		} else {
			apiError.setTimestamp(System.currentTimeMillis());
			apiError.setStatusCode(500);
			apiError.setMessage("Internal server error");
			apiError.setPath(request.getRequestURI());
			apiError.setError("...error...");
		}
		request.setAttribute("error", apiError);

		request.getRequestDispatcher("/WEB-INF/jsp/error/Error.jsp").forward(request, response);
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
