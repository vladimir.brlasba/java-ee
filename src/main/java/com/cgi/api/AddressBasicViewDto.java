package com.cgi.api;

public class AddressBasicViewDto {

	private Long id;
	private String city;
	private String street;
	private Long houseNumber;
	private String zipCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Long getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(long i) {
		this.houseNumber = i;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

}
