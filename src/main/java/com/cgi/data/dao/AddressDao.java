package com.cgi.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.AddressBasicViewDto;
import com.cgi.exceptions.DataAccessException;

@Stateless
public class AddressDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<AddressBasicViewDto> findAll() {
		List<AddressBasicViewDto> addresses = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement
						.executeQuery("SELECT id_address, city, house_number, street, zip_code FROM address a")) {
			while (rs.next()) {
				AddressBasicViewDto address = new AddressBasicViewDto();
				address.setId(rs.getLong("id_address"));
				address.setCity(rs.getString("city"));
				address.setHouseNumber(rs.getLong("house_number"));
				address.setStreet(rs.getString("street"));
				address.setZipCode(rs.getString("zip_code"));
				addresses.add(address);
			}
			return addresses;
		} catch (SQLException e) {
			throw new DataAccessException("Database error: address select", e);

		}
	}

	public void deleteUser(Long id) {
		String deleteUserSQL = "DELETE FROM address a WHERE a.id_address= ? ";
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(deleteUserSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (Exception e) {
			throw new DataAccessException("Database error: address delete", e);
		}
	}

}
