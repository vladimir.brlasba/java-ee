package com.cgi.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.exceptions.DataAccessException;

@Stateless
public class ContactDao {
	
	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;
	
	public void deleteUser(Long id)
	{
		String deleteUserSQL = "DELETE FROM contact c WHERE c.id_user= ? ";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(deleteUserSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();
		
	}
		catch (SQLException e) {
			throw new DataAccessException("Database error: contact delete", e);
		}
	}

}
