package com.cgi.data.dao;

import java.sql.*;
import java.util.*;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.api.PersonFromCityDto;
import com.cgi.api.PersonNewUserDto;
import com.cgi.exceptions.DataAccessException;

@Stateless
public class PersonDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<PersonBasicViewDto> findAll() {
		List<PersonBasicViewDto> persons = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT id_user, first_name, surname, nickname FROM user u")) {
			while (rs.next()) {
				PersonBasicViewDto person = new PersonBasicViewDto();
				person.setId(rs.getLong("id_user"));
				person.setFamilyName(rs.getString("surname"));
				person.setName(rs.getString("nickname"));
				person.setGivenName(rs.getString("first_name"));
				persons.add(person);
			}
			return persons;
		} catch (SQLException e) {
			throw new DataAccessException("Database error: user select", e);
		}

	}

	public PersonBasicViewDto findByIdBasic(Long id) {
		String findByIdSQL = "SELECT id_user, first_name, surname, nickname FROM user u WHERE u.id_user= ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					PersonBasicViewDto person = new PersonBasicViewDto();
					person.setId(rs.getLong("id_user"));
					person.setFamilyName(rs.getString("surname"));
					person.setName(rs.getString("nickname"));
					person.setGivenName(rs.getString("first_name"));
					return person;
				}
				
			}
		} catch (SQLException e) {
			throw new DataAccessException("Database error: user select", e);
		}
		throw new DataAccessException("Database error: user select");

	}

	public PersonDetailedViewDto findById(Long id) {
		String findByIdSQL = "SELECT u.id_user, u.first_name, u.surname, u.nickname, c.contact, a.city, a.street, a.house_number, a.zip_code FROM user u JOIN address a ON u.id_address = a.id_address JOIN contact c ON u.id_user = c.id_user WHERE u.id_user= ? AND c.id_contact_type = '4'";

		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					PersonDetailedViewDto person = new PersonDetailedViewDto();
					person.setId(rs.getLong("id_user"));
					person.setFamilyName(rs.getString("surname"));
					person.setName(rs.getString("nickname"));
					person.setGivenName(rs.getString("first_name"));
					person.setEmail(rs.getString("contact"));
					person.setCity(rs.getString("city"));
					person.setStreet(rs.getString("street"));
					person.sethNumber(rs.getInt("house_number"));
					person.setZipCode(rs.getString("zip_code"));
					return person;
				}
			}
		} catch (SQLException e) {
			throw new DataAccessException("Database error: user select", e);
		}
		throw new DataAccessException("Database error: user select");

	}

	public List<PersonFromCityDto> findByCity(String city) {
		List<PersonFromCityDto> persons = new ArrayList<>();
		String findByCitySQL = "SELECT u.id_user, u.first_name, u.surname, u.nickname, c.contact FROM user u JOIN address a ON u.id_address = a.id_address JOIN contact c ON u.id_user = c.id_user WHERE a.city= ? AND c.id_contact_type = '4' ORDER BY u.nickname\n";
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(findByCitySQL);) {
			ps.setString(1, city);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					PersonFromCityDto person = new PersonFromCityDto();
					person.setId(rs.getLong("id_user"));
					person.setFamilyName(rs.getString("surname"));
					person.setName(rs.getString("nickname"));
					person.setGivenName(rs.getString("first_name"));
					person.setEmail(rs.getString("contact"));
					persons.add(person);
				}
				return persons;
			}
		} catch (SQLException e) {
			throw new DataAccessException("Database error: user select", e);
		}

	}

	public Long getAddressId(long userId) {
		String findByIdSQL = "SELECT u.id_address FROM user u WHERE u.id_user= ? ";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, userId);
			Long address = null;
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					address = rs.getLong("id_address");
				}
				return address;
			}
		} catch (SQLException e) {
			throw new DataAccessException("Database error: user select", e);
		}

	}

	public void deletePersonById(Long id) {
		String deleteUserSQL = "DELETE FROM user u WHERE u.id_user= ? ";
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(deleteUserSQL);) {
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DataAccessException("Database error: user delete", e);
		}
	}

	public void createUser(PersonNewUserDto newUser) {
		String createUserSQL = "INSERT INTO user u (nickname, first_name, surname) VALUES (?,?,?)";
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(createUserSQL);) {
			ps.setString(1, newUser.getName());
			ps.setString(2, newUser.getGivenName());
			ps.setString(3, newUser.getFamilyName());
			int row = ps.executeUpdate();
			System.out.println(row + " rows updated.");

		} catch (SQLException e) {
			throw new DataAccessException("Database error: user create", e);
		}

	}

	public Long lastInsert() {
		String lastInsert = "SELECT LAST_INSERT_ID()";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(lastInsert);) {
			try (ResultSet rs = ps.executeQuery()) {
				Long last = null;
				if (rs.next()) {
					last = rs.getLong("LAST_INSERT_ID()");
				}
				return last;
			}
		} catch (SQLException e) {
			throw new DataAccessException("Database error: user select", e);

		}

	}

}
