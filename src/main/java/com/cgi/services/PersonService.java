package com.cgi.services;

import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.api.PersonFromCityDto;
import com.cgi.api.PersonNewUserDto;
import com.cgi.data.dao.AddressDao;
import com.cgi.data.dao.ContactDao;
import com.cgi.data.dao.PersonDao;

@Stateless
public class PersonService {

	@EJB
	private PersonDao personDao;
	
	@EJB
	private AddressDao addressDao;
	
	@EJB
	private ContactDao contactDao;

	public List<PersonBasicViewDto> findAll() {
		return personDao.findAll();
	}

	public PersonDetailedViewDto findById(Long id) {
		return personDao.findById(id);
	}

	public List<PersonFromCityDto> findByCity(String city) {
		return personDao.findByCity(city);
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Long deletePersonById(Long id)
	{
		Long address = personDao.getAddressId(id);
		personDao.deletePersonById(id);
		addressDao.deleteUser(address);
		contactDao.deleteUser(id);
		return id;
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PersonBasicViewDto createUser(PersonNewUserDto newUser)
	{
	
		personDao.createUser(newUser);
		Long id =personDao.lastInsert();
		return personDao.findByIdBasic(id);
	}

}
