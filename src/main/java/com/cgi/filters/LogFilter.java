package com.cgi.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class LogFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest requestHttp = (HttpServletRequest) request;
		String authHeader = requestHttp.getHeader("Authorization");
		
		String ipAddress = request.getRemoteAddr();
		String host = request.getRemoteHost();
		int port = request.getRemotePort();
		System.out.println("IP " + ipAddress + System.lineSeparator() + "Host: " + host + System.lineSeparator()
				+ "Port: " + port + System.lineSeparator() + ", Time " + new Date().toString());
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
