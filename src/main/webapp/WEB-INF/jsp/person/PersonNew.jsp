<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<!DOCTYPE html>
<my:pagetemplate title="Person New">
	<jsp:attribute name="body">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>id</th>
					<th>Name</th>
					<th>Given Name</th>
					<th>Family Name</th>
				</tr>
			</thead>
				<tbody>
					<tr>
						<td><c:out value="${person.id}" /></td>
						<td><c:out value="${person.name}" /></td>
						<td><c:out value="${person.givenName}" /></td>
						<td><c:out value="${person.familyName}" /></td>
					</tr>
	
				</tbody>
	
	
			</table>
	
	
	
	</jsp:attribute>
</my:pagetemplate>