<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<!DOCTYPE html>
<my:pagetemplate title="Person Removed">
	<jsp:attribute name="body">
		<h1>Person with id: </h1>
		<h1><c:out value="${deletedPerson}" /></h1>
		<h1>deleted.</h1>
	</jsp:attribute>
</my:pagetemplate>