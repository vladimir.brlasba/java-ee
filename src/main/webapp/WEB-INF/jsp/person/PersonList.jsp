<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Persons">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all people in the system <small></small>
	</h1>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Given name</th>
					<th>Family name</th>
				</tr>
			</thead>
				<tbody>
					<c:forEach items="${persons}" var="person">
						<tr>
							<td><c:out value="${person.id}" /></td>
							<td><c:out value="${person.name}" /></td>
							<td><c:out value="${person.givenName}" /></td>
							<td><c:out value="${person.familyName}" /></td>
							<td><a
								href="${pageContext.request.contextPath}/person/detail?id=${person.id}"
								class="btn btn-primary">More info</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</jsp:attribute>
</my:pagetemplate>