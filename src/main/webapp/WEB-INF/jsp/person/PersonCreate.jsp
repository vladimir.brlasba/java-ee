<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Persons Create">
	<jsp:attribute name="body">
	<!-- Top content -->
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text">
						<p></p>
						<h1>
							<strong>Company 01</strong>
						</h1>
						<div class="description">
							<p></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 form-box">
						<div class="form-top">
							<div class="form-top-left">
								<h3>
									<strong>Create new user</strong>
								</h3>
								<p>
									<strong>Enter your username, family name and given name:</strong>
								</p>
							</div>
							<div class="form-top-right">
								<i class="fa fa-lock"></i>
							</div>
						</div>
						<div class="form-bottom">
							<c:if test="${param.error != null}">
								<div class="alert alert-danger">
									<p>Invalid credentials.</p>
								</div>
							</c:if>
							<c:url value="/person/personNew" var="createUserUrl" />
							<form role="form" action="${createUserUrl}" method="post"
									class="create-user-form">
								<div class="form-group">
									<label class="sr-only" for="nickname">Username</label> <input
											type="text" name="nickname" placeholder="Username..."
											class="form-nickname form-control" id="form-nickname">
								</div>
								<div class="form-group">
									<label class="sr-only" for="givenName">Given name</label> <input
											type="text" name="givenName" placeholder="Given name..."
											class="form-givenName form-control" id="form-givenName">
								</div>
								<div class="form-group">
									<label class="sr-only" for="familyName">Password</label> <input
											type="text" name="familyName" placeholder="Family name..."
											class="form-familyName form-control" id="form-familyName">
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								<button type="submit" class="btn">Create user!</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</jsp:attribute>
</my:pagetemplate>