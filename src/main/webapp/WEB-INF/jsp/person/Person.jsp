<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<!DOCTYPE html>
<my:pagetemplate title="Person">
	<jsp:attribute name="body">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>id</th>
					<th>Name</th>
					<th>Given Name</th>
					<th>Family Name</th>
					<th>City</th>
					<th>Street</th>
					<th>House Number</th>
					<th>Zip code</th>
					<th>Email</th>
				</tr>
			</thead>
				<tbody>
					<tr>
						<td><c:out value="${person.id}" /></td>
						<td><c:out value="${person.name}" /></td>
						<td><c:out value="${person.givenName}" /></td>
						<td><c:out value="${person.familyName}" /></td>
						<td><c:out value="${person.city}" /></td>
						<td><c:out value="${person.street}" /></td>
						<td><c:out value="${person.hNumber}" /></td>
						<td><c:out value="${person.zipCode}" /></td>
						<td><c:out value="${person.email}" /></td>
						<td><a
								href="${pageContext.request.contextPath}/person/remove?id=${person.id}"
								class="btn btn-primary">Delete user</a></td>
					</tr>
	
				</tbody>
	
	
			</table>
	
	
	
	</jsp:attribute>
</my:pagetemplate>