<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<my:pagetemplate title="Login">
	<jsp:attribute name="body">
	<!-- Top content -->
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text">
						<p></p>
						<h1>
							<strong>Company 01</strong>
						</h1>
						<div class="description">
							<p></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 form-box">
						<div class="form-top">
							<div class="form-top-left">
								<h3>
									<strong>Login to our site</strong>
								</h3>
								<p>
									<strong>Enter your username and password to log on:</strong>
								</p>
							</div>
							<div class="form-top-right">
								<i class="fa fa-lock"></i>
							</div>
						</div>
						<div class="form-bottom">
							<c:if test="${param.error != null}">
								<div class="alert alert-danger">
									<p>Invalid username and password.</p>
								</div>
							</c:if>
							<c:if test="${param.logout != null}">
								<div class="alert alert-success">
									<p>You have been logged out successfully.</p>
								</div>
							</c:if>
							<c:url value="/login" var="loginUrl" />
							<form role="form" action="${loginUrl}" method="post"
									class="login-form">
								<div class="form-group">
									<label class="sr-only" for="username">Username</label> <input
											type="text" name="username" placeholder="Username..."
											class="form-username form-control" id="form-username">
								</div>
								<div class="form-group">
									<label class="sr-only" for="password">Password</label> <input
											type="password" name="password" placeholder="Password..."
											class="form-password form-control" id="form-password">
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								<button type="submit" class="btn">Sign in!</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</jsp:attribute>
</my:pagetemplate>