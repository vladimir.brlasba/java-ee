<div class="header">
	<nav class="navbar navbar-inverse">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="${pageContext.request.contextPath}/">Company 01</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a
					href="${pageContext.request.contextPath}/">Company 01</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Training <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${pageContext.request.contextPath}/person/list">People</a></li>
						<li><a href="${pageContext.request.contextPath}/person/personCreate">Create user</a></li>
					</ul></li>
				<li><a href="${pageContext.request.contextPath}/city/brno">Brno</a></li>
				<li><a href="${pageContext.request.contextPath}/city/praha">Praha</a></li>
				<li><a href="${pageContext.request.contextPath}/city/plzen">Plzen</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="${pageContext.request.contextPath}/login"><span
						class="glyphicon glyphicon-log-in"> Login</span></a></li>
			</ul>
		</div>
	</nav>
</div>